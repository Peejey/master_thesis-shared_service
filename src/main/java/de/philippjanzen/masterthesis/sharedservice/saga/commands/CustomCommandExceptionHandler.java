package de.philippjanzen.masterthesis.sharedservice.saga.commands;

import java.util.List;

import de.philippjanzen.masterthesis.sharedservice.saga.producers.SagaCustomReplyMessageBuilder;
import io.eventuate.tram.commands.common.CommandReplyOutcome;
import io.eventuate.tram.commands.common.ReplyMessageHeaders;
import io.eventuate.tram.commands.consumer.CommandExceptionHandler;
import io.eventuate.tram.messaging.common.Message;
import io.eventuate.tram.messaging.producer.MessageBuilder;

public class CustomCommandExceptionHandler extends CommandExceptionHandler {

	public CustomCommandExceptionHandler() {
	}

	@Override
	public List<Message> invoke(Throwable cause) {
		return List.of(new SagaCustomReplyMessageBuilder().withFailure().build());
	}

}
