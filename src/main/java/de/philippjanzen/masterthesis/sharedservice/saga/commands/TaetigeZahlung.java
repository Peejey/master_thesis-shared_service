package de.philippjanzen.masterthesis.sharedservice.saga.commands;

import de.philippjanzen.masterthesis.sharedservice.dtos.ZahlungsinformationDTO;
import io.eventuate.tram.commands.common.Command;

public class TaetigeZahlung implements Command {

	private String transactionId;
	private ZahlungsinformationDTO zahlung;

	public TaetigeZahlung() {
	}

	public TaetigeZahlung(String transactionId, ZahlungsinformationDTO zahlung) {
		this.transactionId = transactionId;
		// TODO Auto-generated constructor stub
		this.zahlung = zahlung;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public ZahlungsinformationDTO getZahlung() {
		return zahlung;
	}

}
