package de.philippjanzen.masterthesis.sharedservice.saga.replies;

public class TaetigeZahlungAntwort {

	private String zahlungId;

	public TaetigeZahlungAntwort() {
	}

	public TaetigeZahlungAntwort(String zahlungId) {
		this.zahlungId = zahlungId;
	}

	public String getZahlungId() {
		return zahlungId;
	}

	public void setZahlungId(String zahlungId) {
		this.zahlungId = zahlungId;
	}

}
