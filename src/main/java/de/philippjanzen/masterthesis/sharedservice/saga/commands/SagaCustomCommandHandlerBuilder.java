package de.philippjanzen.masterthesis.sharedservice.saga.commands;

import java.util.List;
import java.util.Optional;
import java.util.function.BiFunction;
import java.util.function.Consumer;
import java.util.function.Function;

import io.eventuate.tram.commands.consumer.CommandHandlers;
import io.eventuate.tram.commands.consumer.CommandMessage;
import io.eventuate.tram.commands.consumer.PathVariables;
import io.eventuate.tram.messaging.common.Message;
import io.eventuate.tram.sagas.common.LockTarget;
import io.eventuate.tram.sagas.participant.PostLockFunction;
import io.eventuate.tram.sagas.participant.SagaCommandHandler;
import io.eventuate.tram.sagas.participant.SagaCommandHandlerBuilder;
import io.eventuate.tram.sagas.participant.SagaCommandHandlersBuilder;

public class SagaCustomCommandHandlerBuilder<C> extends SagaCommandHandlerBuilder<C> {
	private final SagaCustomCommandHandlersBuilder parent;
	private final SagaCommandHandler h;

	public SagaCustomCommandHandlerBuilder(SagaCustomCommandHandlersBuilder parent, SagaCommandHandler h) {
		super(parent, h);
		this.parent = parent;
		this.h = h;
	}

	@Override
	public <C> SagaCustomCommandHandlerBuilder<C> onMessageReturningMessages(Class<C> commandClass,
			Function<CommandMessage<C>, List<Message>> handler) {
		return parent.onMessageReturningMessages(commandClass, handler);
	}

	@Override
	public <C> SagaCustomCommandHandlerBuilder<C> onMessageReturningOptionalMessage(Class<C> commandClass,
			Function<CommandMessage<C>, Optional<Message>> handler) {
		return parent.onMessageReturningOptionalMessage(commandClass, handler);
	}

	@Override
	public <C> SagaCustomCommandHandlerBuilder<C> onMessage(Class<C> commandClass,
			Function<CommandMessage<C>, Message> handler) {
		return parent.onMessage(commandClass, handler);
	}

	@Override
	public <C> SagaCustomCommandHandlerBuilder<C> onMessage(Class<C> commandClass,
			Consumer<CommandMessage<C>> handler) {
		return parent.onMessage(commandClass, handler);
	}

	public SagaCustomCommandHandlerBuilder<C> withPreLock(
			BiFunction<CommandMessage<C>, PathVariables, LockTarget> preLock) {
		h.setPreLock((raw, pvs) -> preLock.apply(raw, pvs));
		return this;
	}

	public SagaCustomCommandHandlerBuilder<C> withPostLock(PostLockFunction<C> postLock) {
		h.setPostLock((raw, pvs, m) -> postLock.apply(raw, pvs, m));
		return this;
	}

	public CustomCommandHandlers build() {
		return parent.build();
	}

}
