package de.philippjanzen.masterthesis.sharedservice.saga.consumers;

import java.util.Set;
import java.util.concurrent.Flow.Subscriber;
import java.util.concurrent.Flow.Subscription;
import java.util.function.Consumer;

import org.springframework.beans.factory.annotation.Autowired;

import de.philippjanzen.masterthesis.sharedservice.messaging.management.ChannelManager;
import de.philippjanzen.masterthesis.sharedservice.messaging.management.ChannelSubscriber;
import de.philippjanzen.masterthesis.sharedservice.saga.SagaCustomMessage;
import io.eventuate.tram.messaging.common.Message;
import io.eventuate.tram.messaging.consumer.MessageConsumer;
import io.eventuate.tram.messaging.consumer.MessageHandler;

public class MessageConsumerImpl implements MessageConsumer {

	@Autowired
	private ChannelManager<SagaCustomMessage> queueManager;

	@Override
	public void subscribe(String subscriberId, Set<String> channels, MessageHandler handler) {
		queueManager.subscribe(new ChannelSubscriber<SagaCustomMessage>(subscriberId,
				(message) -> handler.accept(message.getMessage())), channels);
	}

}
