package de.philippjanzen.masterthesis.sharedservice.saga.commands;

import io.eventuate.tram.commands.common.Command;

public class LeereWarenkorbZurueck implements Command {

	private String transactionId;
	private String warenkorbId;

	public LeereWarenkorbZurueck() {
	}

	public LeereWarenkorbZurueck(String transactionId, String warenkorbId) {
		this.transactionId = transactionId;
		this.warenkorbId = warenkorbId;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public String getWarenkorbId() {
		return warenkorbId;
	}

}
