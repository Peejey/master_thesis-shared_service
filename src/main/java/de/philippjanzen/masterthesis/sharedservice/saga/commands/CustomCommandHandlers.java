package de.philippjanzen.masterthesis.sharedservice.saga.commands;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import io.eventuate.tram.commands.consumer.CommandExceptionHandler;
import io.eventuate.tram.commands.consumer.CommandHandler;
import io.eventuate.tram.commands.consumer.CommandHandlers;

public class CustomCommandHandlers extends CommandHandlers {
	private List<CommandExceptionHandler> exceptionHandlers = new ArrayList<>();

	public CustomCommandHandlers(List<CommandHandler> handlers) {
		super(handlers);
		exceptionHandlers.add(new CustomCommandExceptionHandler());
	}

	@Override
	public Optional<CommandExceptionHandler> findExceptionHandler(Throwable cause) {
		cause.printStackTrace();
		return Optional.ofNullable(exceptionHandlers.get(0));
	}

}
