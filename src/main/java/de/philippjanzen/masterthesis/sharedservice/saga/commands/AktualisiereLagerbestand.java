package de.philippjanzen.masterthesis.sharedservice.saga.commands;

import java.util.List;

import de.philippjanzen.masterthesis.sharedservice.dtos.LagerbestandsAktualisierungDTO;
import io.eventuate.tram.commands.common.Command;

public class AktualisiereLagerbestand implements Command {

	private List<LagerbestandsAktualisierungDTO> lagerbestaende;
	private String transactionId;

	public AktualisiereLagerbestand() {
	}

	public AktualisiereLagerbestand(String transactionId, List<LagerbestandsAktualisierungDTO> lagerbestaende) {
		this.transactionId = transactionId;
		this.lagerbestaende = lagerbestaende;
	}

	public List<LagerbestandsAktualisierungDTO> getLagerbestaende() {
		return lagerbestaende;
	}

	public String getTransactionId() {
		return transactionId;
	}

}
