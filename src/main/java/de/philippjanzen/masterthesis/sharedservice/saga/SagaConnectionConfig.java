package de.philippjanzen.masterthesis.sharedservice.saga;

public class SagaConnectionConfig {

	public static final String BESTELLUNG_SERVICE_CHANNEL = "de.philippjanzen.masterthesis.bestellungservice.saga.BestellungServiceChannel";
	public static final String PRODUKT_SERVICE_CHANNEL = "de.philippjanzen.masterthesis.produktservice.saga.ProduktServiceChannel";
	public static final String WARENKORB_SERVICE_CHANNEL = "de.philippjanzen.masterthesis.warenkorbservice.saga.WarenkorbServiceChannel";
	public static final String ZAHLUNG_SERVICE_CHANNEL = "de.philippjanzen.masterthesis.zahlungservice.saga.ZahlungServiceChannel";
	public static final String ERSTELLE_BESTELLUNG_REPLY_CHANNEL = "de.philippjanzen.masterthesis.bestellungservice.saga.ErstelleBestellungSaga-reply";
	public static final String MICROSERVICES_SAGA_TOPIC_EXCHANGE = "de.philippjanzen.masterthesis.microservices.rabbitmq.sagatopicexchange";
	public static final String PREFIX = "de.philippjanzen.masterthesis.microservices.rabbitmq.prefix";
}
