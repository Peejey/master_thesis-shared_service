package de.philippjanzen.masterthesis.sharedservice.saga.producers;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;

import io.eventuate.javaclient.commonimpl.JSonMapper;
import io.eventuate.javaclient.spring.jdbc.IdGenerator;
import io.eventuate.tram.commands.common.CommandReplyOutcome;
import io.eventuate.tram.commands.common.Failure;
import io.eventuate.tram.commands.common.ReplyMessageHeaders;
import io.eventuate.tram.commands.common.Success;
import io.eventuate.tram.messaging.common.Message;
import io.eventuate.tram.messaging.producer.MessageBuilder;
import io.eventuate.tram.sagas.common.LockTarget;
import io.eventuate.tram.sagas.participant.SagaReplyMessage;

public class SagaCustomReplyMessageBuilder extends MessageBuilder {

	private LockTarget lockTarget;

	public SagaCustomReplyMessageBuilder() {
	}

	public SagaCustomReplyMessageBuilder withLock(Class type, String targetId) {
		lockTarget = new LockTarget(type, targetId);
		return this;
	}

	public SagaCustomReplyMessageBuilder withSuccess(Object reply) {
		this.body = JSonMapper.toJson(reply);
		withHeader(ReplyMessageHeaders.REPLY_OUTCOME, CommandReplyOutcome.SUCCESS.name());
		withHeader(ReplyMessageHeaders.REPLY_TYPE, reply.getClass().getName());
		return this;
	}

	public SagaCustomReplyMessageBuilder withSuccess() {
		return withSuccess(new Success());
	}

	public SagaCustomReplyMessageBuilder withFailure() {
		return withFailure(new Failure());
	}

	private SagaCustomReplyMessageBuilder withFailure(Object reply) {
		this.body = JSonMapper.toJson(reply);
		withHeader(ReplyMessageHeaders.REPLY_OUTCOME, CommandReplyOutcome.FAILURE.name());
		withHeader(ReplyMessageHeaders.REPLY_TYPE, reply.getClass().getName());
		return this;
	}

	public SagaCustomReplyMessageBuilder withId(String id) {
		withHeader(Message.ID, id);
		return this;
	}

	public SagaReplyMessage build() {
		return new SagaReplyMessage(body, headers, Optional.ofNullable(lockTarget));
	}

}
