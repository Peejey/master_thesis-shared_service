package de.philippjanzen.masterthesis.sharedservice.saga.commands;

import io.eventuate.tram.commands.common.Command;

public class TaetigeGutschrift implements Command {

	private String transactionId;
	private String zahlungId;

	public TaetigeGutschrift() {
	}

	public TaetigeGutschrift(String transactionId, String zahlungId) {
		this.transactionId = transactionId;
		this.zahlungId = zahlungId;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public String getZahlungId() {
		return zahlungId;
	}

}
