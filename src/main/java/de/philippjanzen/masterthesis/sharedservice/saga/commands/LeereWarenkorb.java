package de.philippjanzen.masterthesis.sharedservice.saga.commands;

import io.eventuate.tram.commands.common.Command;

public class LeereWarenkorb implements Command {

	private String transactionId;
	private String warenkorbId;

	public LeereWarenkorb() {
	}

	public LeereWarenkorb(String transactionId, String warenkorbId) {
		this.transactionId = transactionId;
		this.warenkorbId = warenkorbId;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public String getWarenkorbId() {
		return warenkorbId;
	}

}
