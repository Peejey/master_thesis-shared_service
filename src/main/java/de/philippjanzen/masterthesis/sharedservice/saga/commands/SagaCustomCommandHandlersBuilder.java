package de.philippjanzen.masterthesis.sharedservice.saga.commands;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.function.Consumer;
import java.util.function.Function;

import io.eventuate.tram.commands.consumer.CommandHandler;
import io.eventuate.tram.commands.consumer.CommandMessage;
import io.eventuate.tram.messaging.common.Message;
import io.eventuate.tram.sagas.participant.SagaCommandHandler;
import io.eventuate.tram.sagas.participant.SagaCommandHandlerBuilder;
import io.eventuate.tram.sagas.participant.SagaCommandHandlersBuilder;

public class SagaCustomCommandHandlersBuilder extends SagaCommandHandlersBuilder {

	private String channel;

	private List<CommandHandler> handlers = new ArrayList<>();

	public static SagaCustomCommandHandlersBuilder fromChannel(String channel) {
		return new SagaCustomCommandHandlersBuilder().andFromChannel(channel);
	}

	private SagaCustomCommandHandlersBuilder andFromChannel(String channel) {
		this.channel = channel;
		return this;
	}

	@Override
	public <C> SagaCustomCommandHandlerBuilder<C> onMessageReturningMessages(Class<C> commandClass,
			Function<CommandMessage<C>, List<Message>> handler) {
		SagaCommandHandler h = new SagaCommandHandler(channel, commandClass, handler);
		this.handlers.add(h);
		return new SagaCustomCommandHandlerBuilder<C>(this, h);
	}

	@Override
	public <C> SagaCustomCommandHandlerBuilder<C> onMessageReturningOptionalMessage(Class<C> commandClass,
			Function<CommandMessage<C>, Optional<Message>> handler) {
		SagaCommandHandler h = new SagaCommandHandler(channel, commandClass,
				(c) -> handler.apply(c).map(Collections::singletonList).orElse(Collections.EMPTY_LIST));
		this.handlers.add(h);
		return new SagaCustomCommandHandlerBuilder<C>(this, h);
	}

	@Override
	public <C> SagaCustomCommandHandlerBuilder<C> onMessage(Class<C> commandClass,
			Function<CommandMessage<C>, Message> handler) {
		SagaCommandHandler h = new SagaCommandHandler(channel, commandClass,
				(c) -> Collections.singletonList(handler.apply(c)));
		this.handlers.add(h);
		return new SagaCustomCommandHandlerBuilder<C>(this, h);
	}

	@Override
	public <C> SagaCustomCommandHandlerBuilder<C> onMessage(Class<C> commandClass,
			Consumer<CommandMessage<C>> handler) {
		SagaCommandHandler h = new SagaCommandHandler(channel, commandClass, (c) -> {
			handler.accept(c);
			return Collections.emptyList();
		});
		this.handlers.add(h);
		return new SagaCustomCommandHandlerBuilder<C>(this, h);
	}

	public CustomCommandHandlers build() {
		return new CustomCommandHandlers(handlers);
	}

}
