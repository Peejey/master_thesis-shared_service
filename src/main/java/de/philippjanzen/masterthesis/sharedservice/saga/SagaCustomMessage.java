package de.philippjanzen.masterthesis.sharedservice.saga;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import org.apache.commons.lang.builder.ToStringBuilder;
import org.springframework.beans.factory.annotation.Autowired;

import com.fasterxml.jackson.annotation.JsonProperty;

import de.philippjanzen.masterthesis.sharedservice.messaging.management.Identifiable;
import io.eventuate.javaclient.spring.jdbc.IdGenerator;
import io.eventuate.tram.messaging.common.Message;
import io.eventuate.tram.messaging.producer.MessageBuilder;

public class SagaCustomMessage implements Serializable, Identifiable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -7873081921427690195L;
	@JsonProperty("payload")
	public String payload;
	@JsonProperty("headers")
	public Map<String, String> headers;

	public SagaCustomMessage() {
	}

	public SagaCustomMessage(Message message) {
		if (!message.hasHeader(Message.ID)) {
			message.setHeader(Message.ID, UUID.randomUUID().toString());
		}
		payload = message.getPayload();
		headers = message.getHeaders();
	}

	public Message getMessage() {
		return MessageBuilder.withPayload(payload).withExtraHeaders("", headers).build();
	}

	@Override
	public String getId() {
		return headers.get(Message.ID);
	}

}
