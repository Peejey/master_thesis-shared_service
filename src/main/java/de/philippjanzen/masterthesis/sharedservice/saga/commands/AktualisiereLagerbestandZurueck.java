package de.philippjanzen.masterthesis.sharedservice.saga.commands;

import java.util.List;

import de.philippjanzen.masterthesis.sharedservice.dtos.LagerbestandsAktualisierungDTO;
import io.eventuate.tram.commands.common.Command;

public class AktualisiereLagerbestandZurueck implements Command {

	private List<LagerbestandsAktualisierungDTO> lagerbestaende;
	private String transactionId;

	public AktualisiereLagerbestandZurueck() {
	}

	public AktualisiereLagerbestandZurueck(String transactionId, List<LagerbestandsAktualisierungDTO> lagerbestaende) {
		this.transactionId = transactionId;
		this.lagerbestaende = lagerbestaende;
	}

	public String getTransactionId() {
		return transactionId;
	}

	public List<LagerbestandsAktualisierungDTO> getLagerbestaende() {
		return lagerbestaende;
	}

}
