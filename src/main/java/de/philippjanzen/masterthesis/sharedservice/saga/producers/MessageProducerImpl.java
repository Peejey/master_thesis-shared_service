package de.philippjanzen.masterthesis.sharedservice.saga.producers;

import org.springframework.amqp.rabbit.core.RabbitTemplate;

import de.philippjanzen.masterthesis.sharedservice.messaging.rabbitmq.AmqpConnectionConfig;
import de.philippjanzen.masterthesis.sharedservice.saga.SagaCustomMessage;
import io.eventuate.tram.commands.common.CommandMessageHeaders;
import io.eventuate.tram.messaging.common.Message;
import io.eventuate.tram.messaging.producer.MessageProducer;
import io.eventuate.tram.sagas.common.SagaCommandHeaders;

public class MessageProducerImpl implements MessageProducer {

	private final RabbitTemplate rabbitTemplate;
	private AmqpConnectionConfig rabbitConnectionConfig;

	public MessageProducerImpl(RabbitTemplate rabbitTemplate, AmqpConnectionConfig rabbitConnectionConfig) {
		this.rabbitConnectionConfig = rabbitConnectionConfig;
		this.rabbitTemplate = rabbitTemplate;
	}

	public void send(String channel, Message message) {
		rabbitTemplate.convertAndSend(rabbitConnectionConfig.getSagaExchange(), channel,
				new SagaCustomMessage(message));
		System.out.println("Sended messsage: id " + message.getId() + " with payload <" + message.getPayload() + ">"
				+ "SAGA_TYPE: " + message.getHeader(SagaCommandHeaders.SAGA_TYPE) + "COMMAND_TYPE: "
				+ message.getHeader(CommandMessageHeaders.COMMAND_TYPE) + "REPLY_TO: "
				+ message.getHeader(CommandMessageHeaders.REPLY_TO) + "DESTINATION"
				+ message.getHeader(CommandMessageHeaders.DESTINATION) + "RESOURCE"
				+ message.getHeader(CommandMessageHeaders.RESOURCE));
	}
}
