package de.philippjanzen.masterthesis.sharedservice.saga.channels;

import de.philippjanzen.masterthesis.sharedservice.messaging.rabbitmq.AmqpConnectionConfig;
import de.philippjanzen.masterthesis.sharedservice.messaging.rabbitmq.AmqpConnectionConfigImpl;
import io.eventuate.tram.commands.common.ChannelMapping;

public class ChannelMappingImpl implements ChannelMapping {

	private AmqpConnectionConfig routingKeys = new AmqpConnectionConfigImpl();

	@Override
	public String transform(String channel) {
		return routingKeys.getRoute(channel);
	}

}
