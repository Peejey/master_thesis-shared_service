package de.philippjanzen.masterthesis.sharedservice.dtos;

public class ZahlungsinformationDTO {

	private String iban;
	private String bic;
	private String kontoinhaber;
	private String bank;
	private String referenzId;
	private double zahlungsSumme;

	public ZahlungsinformationDTO() {
	}

	public ZahlungsinformationDTO(String iban, String bic, String kontoinhaber, String bank, String referenzId,
			double zahlungsSumme) {
		super();
		this.iban = iban;
		this.bic = bic;
		this.kontoinhaber = kontoinhaber;
		this.bank = bank;
		this.referenzId = referenzId;
		this.zahlungsSumme = zahlungsSumme;
	}

	public String getIban() {
		return iban;
	}

	public String getBic() {
		return bic;
	}

	public String getKontoinhaber() {
		return kontoinhaber;
	}

	public String getBank() {
		return bank;
	}

	public String getReferenzId() {
		return referenzId;
	}

	public double getZahlungsSumme() {
		return zahlungsSumme;
	}

	public void setIban(String iban) {
		this.iban = iban;
	}

	public void setBic(String bic) {
		this.bic = bic;
	}

	public void setKontoinhaber(String kontoinhaber) {
		this.kontoinhaber = kontoinhaber;
	}

	public void setBank(String bank) {
		this.bank = bank;
	}

	public void setReferenzId(String referenzId) {
		this.referenzId = referenzId;
	}

	public void setZahlungsSumme(double zahlungsSumme) {
		this.zahlungsSumme = zahlungsSumme;
	}

}
