package de.philippjanzen.masterthesis.sharedservice.messaging.rabbitmq;

import static org.junit.Assert.assertTrue;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;

import de.philippjanzen.masterthesis.sharedservice.events.EventsConnectionConfig;
import de.philippjanzen.masterthesis.sharedservice.saga.SagaConnectionConfig;

public class AmqpConnectionConfigImpl implements AmqpConnectionConfig {
	private static final String CONFIG_FILE_NAME = "rabbit.properties";

	private Properties properties;

	public AmqpConnectionConfigImpl() {
		readConfig();
		validateConfig();
	}

	private void validateConfig() {
		assertTrue(properties.containsKey(ConnectionConfig.HOSTNAME_ENTRY));

		assertTrue(properties.containsKey(EventsConnectionConfig.BESTELLUNG_SERVICE_CHANNEL));
		assertTrue(properties.containsKey(EventsConnectionConfig.PRODUKT_SERVICE_CHANNEL));
		assertTrue(properties.containsKey(EventsConnectionConfig.ZAHLUNG_SERVICE_CHANNEL));
		assertTrue(properties.containsKey(EventsConnectionConfig.WARENKORB_SERVICE_CHANNEL));
		assertTrue(properties.containsKey(EventsConnectionConfig.MICROSERVICES_TOPIC_EXCHANGE));

		assertTrue(properties.containsKey(SagaConnectionConfig.BESTELLUNG_SERVICE_CHANNEL));
		assertTrue(properties.containsKey(SagaConnectionConfig.PRODUKT_SERVICE_CHANNEL));
		assertTrue(properties.containsKey(SagaConnectionConfig.ZAHLUNG_SERVICE_CHANNEL));
		assertTrue(properties.containsKey(SagaConnectionConfig.WARENKORB_SERVICE_CHANNEL));
		assertTrue(properties.containsKey(SagaConnectionConfig.ERSTELLE_BESTELLUNG_REPLY_CHANNEL));
		assertTrue(properties.containsKey(SagaConnectionConfig.MICROSERVICES_SAGA_TOPIC_EXCHANGE));
		assertTrue(properties.containsKey(SagaConnectionConfig.PREFIX));
	}

	private void readConfig() {
		String rootPath = Thread.currentThread().getContextClassLoader().getResource("").getPath();
		String appConfigPath = rootPath + CONFIG_FILE_NAME;

		properties = new Properties();
		try {
			properties.load(new FileInputStream(appConfigPath));
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public String getRoute(String channel) {
		return properties.getProperty(channel);
	}

	public void setRoute(String channel) {
		properties.setProperty(channel, properties.getProperty(EventsConnectionConfig.PREFIX) + channel + ".saga");
	}

	public String getHostname() {
		return properties.getProperty(ConnectionConfig.HOSTNAME_ENTRY);
	}

	@Override
	public String getSagaExchange() {
		return properties.getProperty(SagaConnectionConfig.MICROSERVICES_SAGA_TOPIC_EXCHANGE);
	}

	@Override
	public String getEventsExchange() {
		return properties.getProperty(EventsConnectionConfig.MICROSERVICES_TOPIC_EXCHANGE);
	}

}
