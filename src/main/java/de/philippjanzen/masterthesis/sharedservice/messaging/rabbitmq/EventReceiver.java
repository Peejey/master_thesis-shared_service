package de.philippjanzen.masterthesis.sharedservice.messaging.rabbitmq;

import org.springframework.beans.factory.annotation.Autowired;

import de.philippjanzen.masterthesis.sharedservice.events.DomainEventEnvelope;
import de.philippjanzen.masterthesis.sharedservice.messaging.management.ChannelManager;

public class EventReceiver {

	@Autowired
	ChannelManager<DomainEventEnvelope> channelManager;

	public void receiveEvent(final DomainEventEnvelope domainEventEnvelope) {
		System.out.println("Event received!");
		channelManager.publish(domainEventEnvelope.getDestination(), domainEventEnvelope);
	}
}
