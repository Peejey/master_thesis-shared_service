package de.philippjanzen.masterthesis.sharedservice.messaging.management;

import java.util.Set;
import java.util.concurrent.Flow.Subscriber;

public interface SubscribableMultiple<T> {

	public void subscribe(Subscriber<? super T> subscriber, Set<String> channels);

	public void unsubscribe(Subscriber<? super T> subscriber, Set<String> channels);
}
