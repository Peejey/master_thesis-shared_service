package de.philippjanzen.masterthesis.sharedservice.messaging.management;

import java.util.concurrent.Flow.Subscriber;
import java.util.concurrent.Flow.Subscription;
import java.util.function.Consumer;

public class ChannelSubscriber<T> implements Subscriber<T> {

	private String subscriberId;
	private Consumer<T> consumer;

	public ChannelSubscriber(String subscriberId, Consumer<T> consumer) {
		this.subscriberId = subscriberId;
		this.consumer = consumer;
	}

	@Override
	public void onSubscribe(Subscription subscription) {
	}

	@Override
	public void onNext(T item) {
		consumer.accept(item);
	}

	@Override
	public void onError(Throwable throwable) {
	}

	@Override
	public void onComplete() {
	}

}
