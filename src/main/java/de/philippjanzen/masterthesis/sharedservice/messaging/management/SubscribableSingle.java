package de.philippjanzen.masterthesis.sharedservice.messaging.management;

import java.util.concurrent.Flow.Subscriber;

public interface SubscribableSingle<T> {

	public boolean subscribe(Subscriber<? super T> subscriber);

	public boolean unsubscribe(Subscriber<? super T> subscriber);

	public void publish(T message);
}
