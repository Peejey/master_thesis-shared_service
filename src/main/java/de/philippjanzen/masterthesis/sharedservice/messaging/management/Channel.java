package de.philippjanzen.masterthesis.sharedservice.messaging.management;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Flow.Subscriber;

public class Channel<T extends Identifiable> implements SubscribableSingle<T> {

	private static final int BUFFER_LIMIT = 1000;
	private String channelName;
	private List<Subscriber<? super T>> subscribers = new ArrayList<>();
	private Set<T> undeliveredMessagesBuffer = new HashSet<>();
	private Set<String> processedMessagesBuffer = new HashSet<>();
	private Set<String> processedMessagesBufferToBeDeleted = new HashSet<>();

	protected Channel(String channelName) {
		this.channelName = channelName;
	}

	@Override
	synchronized public boolean unsubscribe(Subscriber<? super T> subscriber) {
		return subscribers.remove(subscriber);
	}

	@Override
	synchronized public void publish(T message) {
		if (validateIsDuplicate(message)) {
			System.out.println("Duplicate Message detected: Processing of message will be skipped!");
			return;
		}
		if (subscribers.isEmpty()) {
			bufferUndeliveredMessage(message);
		} else {
			subscribers.forEach(subscriber -> {
				subscriber.onNext(message);
			});
		}

		bufferProcessedMessages(message);
	}

	private void bufferUndeliveredMessage(T message) {
		if (undeliveredMessagesBuffer.size() < BUFFER_LIMIT) {
			undeliveredMessagesBuffer.add(message);
		} else {
			throw new RuntimeException(
					"Buffer Overflow: UndeliveredMessagesBuffer size of " + BUFFER_LIMIT + " reached.");
		}
	}

	private boolean validateIsDuplicate(T message) {
		return processedMessagesBuffer.contains(message.getId())
				|| processedMessagesBufferToBeDeleted.contains(message.getId());
	}

	private void bufferProcessedMessages(T message) {
		if (processedMessagesBuffer.size() > BUFFER_LIMIT) {
			processedMessagesBufferToBeDeleted = processedMessagesBuffer;
			processedMessagesBuffer = new HashSet<>();
		}
		processedMessagesBuffer.add(message.getId());
	}

	@Override
	synchronized public boolean subscribe(Subscriber<? super T> subscriber) {
		boolean result = subscribers.add(subscriber);
		undeliveredMessagesBuffer.forEach(message -> {
			subscriber.onNext(message);
		});
		undeliveredMessagesBuffer = new HashSet<>();
		return result;
	}

}
