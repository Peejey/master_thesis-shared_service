package de.philippjanzen.masterthesis.sharedservice.messaging.rabbitmq;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import de.philippjanzen.masterthesis.sharedservice.events.DomainEventEnvelope;
import de.philippjanzen.masterthesis.sharedservice.messaging.management.Channel;
import de.philippjanzen.masterthesis.sharedservice.messaging.management.ChannelManager;
import de.philippjanzen.masterthesis.sharedservice.saga.SagaCustomMessage;
import io.eventuate.tram.commands.common.ChannelMapping;
import io.eventuate.tram.commands.common.CommandMessageHeaders;
import io.eventuate.tram.commands.common.ReplyMessageHeaders;
import io.eventuate.tram.messaging.common.Message;
import io.eventuate.tram.sagas.common.SagaCommandHeaders;
import io.eventuate.tram.sagas.common.SagaReplyHeaders;

@Component
public class Receiver {

	private ChannelManager<SagaCustomMessage> sagaChannelManager;
	private ChannelManager<DomainEventEnvelope> eventsChannelManager;

	@Autowired
	private ChannelMapping channelMapping;

	public Receiver(ChannelManager<SagaCustomMessage> sagaChannelManager,
			ChannelManager<DomainEventEnvelope> eventsChannelManager) {
		this.sagaChannelManager = sagaChannelManager;
		this.eventsChannelManager = eventsChannelManager;
	}

	public void receiveMessage(final DomainEventEnvelope domainEventEnvelope) {
		System.out.println("Event received!");
		eventsChannelManager.publish(domainEventEnvelope.getDestination(), domainEventEnvelope);
	}

	public void receiveMessage(final SagaCustomMessage sagaMessage) {

		Message message = sagaMessage.getMessage();

		Optional<String> destinationOpt = getMessageDestination(message);

		if (destinationOpt == null || destinationOpt.isEmpty()) {
			return;
		}

		String channel = destinationOpt.get();
		channel = channelMapping.transform(channel);
		Optional<Channel<SagaCustomMessage>> queueOpt = sagaChannelManager.getChannel(channel);
		if (queueOpt.isPresent()) {
			queueOpt.get().publish(sagaMessage);
		} else {
			sagaChannelManager.setChannel(channel).publish(sagaMessage);
		}
	}

	private Optional<String> getMessageDestination(Message message) {
		Optional<String> destinationOpt = null;
		if (message.hasHeader(SagaCommandHeaders.SAGA_ID)) {
			// is saga command
			destinationOpt = message.getHeader(CommandMessageHeaders.DESTINATION);

			System.out.println("Received command messsage: id " + message.getId() + " with payload <"
					+ message.getPayload() + ">" + "SAGA_TYPE: " + message.getHeader(SagaCommandHeaders.SAGA_TYPE)
					+ "\nCOMMAND_TYPE: " + message.getHeader(CommandMessageHeaders.COMMAND_TYPE) + "REPLY_TO: "
					+ message.getHeader(CommandMessageHeaders.REPLY_TO) + "DESTINATION"
					+ message.getHeader(CommandMessageHeaders.DESTINATION) + "RESOURCE"
					+ message.getHeader(CommandMessageHeaders.RESOURCE) + "\nREPLY_TYPE");

		} else if (message.hasHeader(SagaReplyHeaders.REPLY_SAGA_ID)) {
			// is saga reply
			destinationOpt = message.getHeader(CommandMessageHeaders.inReply(CommandMessageHeaders.REPLY_TO));

			System.out.println("Received reply messsage: id " + message.getId() + " with payload <"
					+ message.getPayload() + ">" + "SAGA_TYPE: "
					+ message.getHeader(CommandMessageHeaders.inReply(SagaCommandHeaders.SAGA_TYPE))
					+ "\nCOMMAND_TYPE: "
					+ message.getHeader(CommandMessageHeaders.inReply(CommandMessageHeaders.COMMAND_TYPE))
					+ "REPLY_TO: " + message.getHeader(CommandMessageHeaders.inReply(CommandMessageHeaders.REPLY_TO))
					+ "DESTINATION"
					+ message.getHeader(CommandMessageHeaders.inReply(CommandMessageHeaders.DESTINATION)) + "RESOURCE"
					+ message.getHeader(CommandMessageHeaders.inReply(CommandMessageHeaders.RESOURCE)) + "\nREPLY_TYPE"
					+ "REPLY_TYPE" + message.getHeader(ReplyMessageHeaders.REPLY_TYPE));
		}
		return destinationOpt;
	}
}