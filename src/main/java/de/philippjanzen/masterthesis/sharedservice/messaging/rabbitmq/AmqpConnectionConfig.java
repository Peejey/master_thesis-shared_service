package de.philippjanzen.masterthesis.sharedservice.messaging.rabbitmq;

public interface AmqpConnectionConfig {

	public String getRoute(String channel);

	public void setRoute(String channel);

	public String getHostname();

	public String getSagaExchange();

	public String getEventsExchange();

}
