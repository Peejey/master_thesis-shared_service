package de.philippjanzen.masterthesis.sharedservice.messaging.management;

import java.util.HashMap;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.Flow.Subscriber;

import org.springframework.stereotype.Service;

@Service
public class ChannelManager<T extends Identifiable> implements SubscribableMultiple<T> {

	private Map<String, Channel<T>> channels = new HashMap<>();

	public ChannelManager() {
	}

	public Optional<Channel<T>> getChannel(String channelName) {
		return Optional.ofNullable(channels.get(channelName));
	}

	public Channel<T> setChannel(String channelName) {
		Channel<T> channel = new Channel<T>(channelName);
		channels.put(channelName, channel);
		return channel;
	}

	@Override
	synchronized public void subscribe(Subscriber<? super T> subscriber, Set<String> channels) {
		channels.forEach(c -> {
			Optional<Channel<T>> channel = getChannel(c);
			if (channel.isPresent()) {
				channel.get().subscribe(subscriber);
			} else {
				setChannel(c).subscribe(subscriber);
			}
		});
	}

	@Override
	synchronized public void unsubscribe(Subscriber<? super T> subscriber, Set<String> channels) {
		channels.forEach(c -> {
			Optional<Channel<T>> channel = getChannel(c);
			if (channel.isPresent()) {
				channel.get().unsubscribe(subscriber);
			}
		});
	}

	synchronized public void publish(String channel, T message) {
		Optional<Channel<T>> optChannel = getChannel(channel);
		if (optChannel.isPresent()) {
			optChannel.get().publish(message);
		} else {
			setChannel(channel).publish(message);
		}
	}

}
