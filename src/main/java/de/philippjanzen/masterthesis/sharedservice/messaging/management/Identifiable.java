package de.philippjanzen.masterthesis.sharedservice.messaging.management;

public interface Identifiable {
	String getId();
}
