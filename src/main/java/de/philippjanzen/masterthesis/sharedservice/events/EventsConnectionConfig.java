package de.philippjanzen.masterthesis.sharedservice.events;

public class EventsConnectionConfig {
	public static final String BESTELLUNG_SERVICE_CHANNEL = "de.philippjanzen.masterthesis.bestellungservice.events.bestellungservicechannel";
	public static final String PRODUKT_SERVICE_CHANNEL = "de.philippjanzen.masterthesis.produktservice.events.produktservicechannel";
	public static final String WARENKORB_SERVICE_CHANNEL = "de.philippjanzen.masterthesis.warenkorbservice.events.warenkorbservicechannel";
	public static final String ZAHLUNG_SERVICE_CHANNEL = "de.philippjanzen.masterthesis.zahlungservice.events.zahlungservicechannel";
	public static final String MICROSERVICES_TOPIC_EXCHANGE = "de.philippjanzen.masterthesis.microservices.rabbitmq.eventstopicexchange";
	public static final String MESSAGE_BROKER_HOSTNAME = "de.philippjanzen.masterthesis.microservices.rabbitmq.hostname";
	public static final String PREFIX = "de.philippjanzen.masterthesis.microservices.rabbitmq.prefix";
}
