package de.philippjanzen.masterthesis.sharedservice.events;

import org.springframework.data.repository.CrudRepository;

public interface DomainEventEnvelopeRepository extends CrudRepository<DomainEventEnvelope, String> {

}
