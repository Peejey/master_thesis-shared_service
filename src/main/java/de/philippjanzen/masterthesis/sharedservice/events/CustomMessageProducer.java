package de.philippjanzen.masterthesis.sharedservice.events;

public interface CustomMessageProducer<T> {

	public void send(String destination, T message);
}
