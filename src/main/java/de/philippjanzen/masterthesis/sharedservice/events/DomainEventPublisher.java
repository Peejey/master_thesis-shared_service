package de.philippjanzen.masterthesis.sharedservice.events;

import java.util.List;

public interface DomainEventPublisher<T extends DomainEvent> {

	void publish(Class<? extends DomainAggregate> class1, String aggregateId, String correlationId,
			List<T> domainEvents);

}
