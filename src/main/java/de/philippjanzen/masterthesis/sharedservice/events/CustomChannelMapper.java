package de.philippjanzen.masterthesis.sharedservice.events;

public interface CustomChannelMapper {

	String transform(String channel);
}
