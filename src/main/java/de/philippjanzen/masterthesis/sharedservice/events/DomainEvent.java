package de.philippjanzen.masterthesis.sharedservice.events;

public abstract class DomainEvent {

	public abstract String getChannel();
}
