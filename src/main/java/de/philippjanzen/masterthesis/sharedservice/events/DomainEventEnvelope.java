package de.philippjanzen.masterthesis.sharedservice.events;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.Base64;
import java.util.UUID;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Lob;

import com.fasterxml.jackson.annotation.JsonProperty;

import de.philippjanzen.masterthesis.sharedservice.messaging.management.Identifiable;

@Entity
public class DomainEventEnvelope implements Identifiable, Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8233266065642622432L;

	@Id
	@JsonProperty
	private String id;
	@JsonProperty
	private String timestamp = LocalDateTime.now().toString();
	@JsonProperty
	private String eventType;
	@JsonProperty
	private String aggregateType;
	@JsonProperty
	private String aggregateId;
	@JsonProperty
	private String correlationId;
	@JsonProperty
	private String destination;
	@JsonProperty
	@Lob
	private String eventAsJson;

	protected DomainEventEnvelope() {
	}

	public DomainEventEnvelope(String eventType, String aggregateType, String aggregateId, String correlationId,
			String destination, String eventAsJson) {
		id = UUID.randomUUID().toString();
		this.eventType = eventType;
		this.aggregateType = aggregateType;
		this.aggregateId = aggregateId;
		this.correlationId = correlationId;
		this.destination = destination;
		this.eventAsJson = Base64.getEncoder().encodeToString(eventAsJson.getBytes());
	}

	public String getTimestamp() {
		return timestamp;
	}

	public String getEventType() {
		return eventType;
	}

	public String getAggregateType() {
		return aggregateType;
	}

	public String getAggregateId() {
		return aggregateId;
	}

	public String getCorrelationId() {
		return correlationId;
	}

	public String getDestination() {
		return destination;
	}

	public String getEvent() {
		return new String(Base64.getDecoder().decode(eventAsJson.getBytes()));
	}

	@Override
	public String getId() {
		return id;
	}

}
