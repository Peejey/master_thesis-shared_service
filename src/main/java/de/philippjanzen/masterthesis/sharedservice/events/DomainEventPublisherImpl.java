package de.philippjanzen.masterthesis.sharedservice.events;

import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.springframework.context.ApplicationEventPublisher;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class DomainEventPublisherImpl<T extends DomainEvent> implements DomainEventPublisher<T> {

	private DomainEventEnvelopeRepository domainEventEnvelopeRepository;
	private ObjectMapper objectMapper;
	private ApplicationEventPublisher applicationEventPublisher;

	public DomainEventPublisherImpl(DomainEventEnvelopeRepository domainEventEnvelopeRepository,
			ObjectMapper objectMapper, ApplicationEventPublisher applicationEventPublisher) {
		this.domainEventEnvelopeRepository = domainEventEnvelopeRepository;
		this.objectMapper = objectMapper;
		this.applicationEventPublisher = applicationEventPublisher;
	}

	private Set<DomainEventEnvelope> makeEnvelopes(Class<? extends DomainAggregate> aggregateType, String aggregateId,
			String correlationId, List<T> domainEvents) {
		Set<DomainEventEnvelope> envelopes = new LinkedHashSet<>();
		domainEvents.forEach(event -> {
			try {
				envelopes.add(new DomainEventEnvelope(event.getClass().getCanonicalName(),
						aggregateType.getCanonicalName(), aggregateId, correlationId, event.getChannel(),
						objectMapper.writeValueAsString(event)));
			} catch (JsonProcessingException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		});
		return envelopes;
	}

	@Override
	public void publish(Class<? extends DomainAggregate> aggregateType, String aggregateId, String correlationId,
			List<T> domainEvents) {
		Set<DomainEventEnvelope> envelopes = makeEnvelopes(aggregateType, aggregateId, correlationId, domainEvents);
		domainEventEnvelopeRepository.saveAll(envelopes);
		applicationEventPublisher.publishEvent(new DomainEventOccurred(aggregateType));
	}

}
