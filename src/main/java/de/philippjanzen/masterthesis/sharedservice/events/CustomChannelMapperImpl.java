package de.philippjanzen.masterthesis.sharedservice.events;

import de.philippjanzen.masterthesis.sharedservice.messaging.rabbitmq.AmqpConnectionConfig;

public class CustomChannelMapperImpl implements CustomChannelMapper {

	private AmqpConnectionConfig rabbitmqConnectionConfig;

	public CustomChannelMapperImpl(AmqpConnectionConfig rabbitmqConnectionConfig) {
		this.rabbitmqConnectionConfig = rabbitmqConnectionConfig;

	}

	@Override
	public String transform(String channel) {
		return rabbitmqConnectionConfig.getRoute(channel);
	}

}
