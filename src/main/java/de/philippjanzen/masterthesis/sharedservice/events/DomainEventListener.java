package de.philippjanzen.masterthesis.sharedservice.events;

import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import org.springframework.transaction.event.TransactionPhase;
import org.springframework.transaction.event.TransactionalEventListener;

import de.philippjanzen.masterthesis.sharedservice.events.CustomMessageProducer;
import de.philippjanzen.masterthesis.sharedservice.events.DomainEventEnvelope;
import de.philippjanzen.masterthesis.sharedservice.events.DomainEventEnvelopeRepository;
import de.philippjanzen.masterthesis.sharedservice.events.DomainEventOccurred;

@Component
public class DomainEventListener {

	private CustomMessageProducer<DomainEventEnvelope> messageProducer;
	private DomainEventEnvelopeRepository domainEventEnvelopeRepository;

	public DomainEventListener(DomainEventEnvelopeRepository domainEventEnvelopeRepository,
			CustomMessageProducer<DomainEventEnvelope> messageProducer) {
		this.domainEventEnvelopeRepository = domainEventEnvelopeRepository;
		this.messageProducer = messageProducer;
		processEvents();
	}

	@EventListener(DomainEventOccurred.class)
	@TransactionalEventListener(phase = TransactionPhase.AFTER_COMMIT)
	public void onDomainEventsOccurred(DomainEventOccurred event) {
		processEvents();
	}

	private void processEvents() {
		domainEventEnvelopeRepository.findAll().forEach(envelope -> {
			messageProducer.send(envelope.getDestination(), envelope);
			domainEventEnvelopeRepository.deleteById(envelope.getId());
		});
	}
}
