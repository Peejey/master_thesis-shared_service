package de.philippjanzen.masterthesis.sharedservice.events;

import org.springframework.amqp.rabbit.core.RabbitTemplate;

import de.philippjanzen.masterthesis.sharedservice.events.CustomChannelMapper;
import de.philippjanzen.masterthesis.sharedservice.messaging.rabbitmq.AmqpConnectionConfig;

public class CustomMessageProducerImpl<T> implements CustomMessageProducer<T> {

	private final RabbitTemplate rabbitTemplate;
	private AmqpConnectionConfig rabbitConnectionConfig;
	private CustomChannelMapper channelMapping;

	public CustomMessageProducerImpl(RabbitTemplate rabbitTemplate, AmqpConnectionConfig rabbitConnectionConfig,
			CustomChannelMapper channelMapping) {
		this.rabbitConnectionConfig = rabbitConnectionConfig;
		this.rabbitTemplate = rabbitTemplate;
		this.channelMapping = channelMapping;
	}

	@Override
	public void send(String destination, T message) {
		rabbitTemplate.convertAndSend(rabbitConnectionConfig.getEventsExchange(), channelMapping.transform(destination),
				message);
		System.out.println("Event sended");
	}
}
