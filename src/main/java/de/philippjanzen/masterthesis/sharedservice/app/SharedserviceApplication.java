package de.philippjanzen.masterthesis.sharedservice.app;


import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SharedserviceApplication {

	public static void main(String[] args) {
		SpringApplication.run(SharedserviceApplication.class, args);
	}

}

